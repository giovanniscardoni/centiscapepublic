package org.cytoscape.centiscape.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.JPanel;

import org.cytoscape.centiscape.internal.betweenness.BetweennessMethods;
import org.cytoscape.centiscape.internal.betweenness.DirectedEdgeBetweenness;
import org.cytoscape.centiscape.internal.betweenness.FinalResultBetweenness;
import org.cytoscape.centiscape.internal.centralities.NodeCentrality;
import org.cytoscape.centiscape.internal.centroid.CentroidMethods;
import org.cytoscape.centiscape.internal.centroid.FinalResultCentroid;
import org.cytoscape.centiscape.internal.closeness.DirectedCloseness;
import org.cytoscape.centiscape.internal.eccentricity.DirectedEccentricity;
import org.cytoscape.centiscape.internal.eigenVector.CalculateEigenVector;
import org.cytoscape.centiscape.internal.radiality.DirectedRadiality;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

/**
 * @author faizi
 */

public class DirectedAlgorithm {
    public CyNetwork network;
    public boolean[] checkedCentralities;
    public String[] directedCentralities;
    public JPanel menu;
    public static boolean stop;
    public boolean openResultsPanel;
    public boolean displayBetweennessResults = false;
    private SortedMap<Long, Double> stressMap = new TreeMap<Long, Double>();
    public double Diameter = 0.0;
    public double totalDist;
    //dataStructures
    public Map<CyNode, List<ShortestPathList>> singleShortestPaths;
    public Map<CyNode,Double> directedOutDegreeValues;
    public Map<CyNode,Double> directedInDegreeValues;
    public Map<CyNode,Double> directedClosenessValues;
    public Map<CyNode,Double> directedEccentrityValues;
    public Map<CyNode,Double> directedRadialityValues;
    public Map<CyNode,Double> directedStressValues;
    public Map<CyNode,Double> directedBridgingValues;
    public DirectedEdgeBetweenness edgeBetweenness;
    private static Vector BetweennessVectorResults;
    private static Vector CentroidVectorResults;
    private static Vector CentroidVectorofNodes;
    public double[][] adjacencyMatrixOfNetwork;
    public Core centiscapecore;
    public Vector centralities = new Vector();
    
    public static void stopAlgo(){
        stop = true;
    }

    public void executeCentralities(CyNetwork n, boolean[] c, String[] d, CentiScaPeStartMenu menu, Core core){
        stop = false;
        openResultsPanel = false;
        network = n;
        checkedCentralities = c;
        directedCentralities = d;
        this.centiscapecore = core;
        
        int totalnodecount = network.getNodeCount();
        int totalEdgeCount = network.getEdgeCount();
        int nodeworked = 0;
        CyTable nodeTable = network.getDefaultNodeTable();
        CyTable networkTable = network.getDefaultNetworkTable();
        List<CyNode> nodeList = network.getNodeList();
        List<ShortestPathList> shortestPaths = null;
        
        if(checkedCentralities[7])
            displayBetweennessResults = true;
        else
            displayBetweennessResults = false;

        if(checkedCentralities[10] && !checkedCentralities[7])
            checkedCentralities[7] = true;
        
        //instantiate data stuctures
        if(checkedCentralities[1])
            totalDist = 0.0;

        if(checkedCentralities[2]){
            //Degree checkbox is selected
            directedOutDegreeValues = new HashMap<CyNode, Double>(totalnodecount);
            directedInDegreeValues = new HashMap<CyNode, Double>(totalnodecount);
        }
        if(checkedCentralities[3])
            directedEccentrityValues = new HashMap<CyNode,Double>(totalnodecount);

        if(checkedCentralities[4])
            directedRadialityValues = new HashMap<CyNode, Double>(totalnodecount);

        if(checkedCentralities[5])
            directedClosenessValues = new HashMap<CyNode,Double>(totalnodecount);

        if(checkedCentralities[6])
            directedStressValues = new HashMap<CyNode,Double>(totalnodecount);

        if(checkedCentralities[7]){
            BetweennessVectorResults = new Vector();
            for (Iterator i = nodeList.iterator(); i.hasNext();) {
                if (stop) {
                    return;
                }
                CyNode root = (CyNode) i.next();
                BetweennessVectorResults.add(new FinalResultBetweenness(root, 0));
            }
        }
        if (checkedCentralities[8]) {
                CentroidVectorResults = new Vector();
                CentroidVectorofNodes = new Vector();
                for (Iterator i = nodeList.iterator(); i.hasNext();) {
                    if (stop)
                     return;

                    CyNode root = (CyNode) i.next();
                    CentroidVectorofNodes.addElement(root.getSUID());
                }
        }
        if(checkedCentralities[9])
            adjacencyMatrixOfNetwork = new double[totalnodecount][totalnodecount];

        if(checkedCentralities[10])
            directedBridgingValues = new HashMap<CyNode, Double>(totalnodecount);

        if(checkedCentralities[11])
        	edgeBetweenness = new DirectedEdgeBetweenness(network);

        boolean StressisOn = true;
        stressMap.clear();
        for (CyNode root : nodeList) {
        	stressMap.put(root.getSUID(), new Double(0));
        }
        int k=0;
        singleShortestPaths = new HashMap<CyNode, List<ShortestPathList>>();
        for (CyNode root : nodeList) {
        	if (stop)
        		return;

        	shortestPaths = new ShortestPathsPerNode(network, root, stressMap, StressisOn, true, menu.isWeighted).getPaths();

            // create a single shortest path list
            List<ShortestPathList> shortestPaths2 = new ArrayList<ShortestPathList>();
            Set<String> seen = new HashSet<String>();
            for (ShortestPathList path: shortestPaths)
            	if (seen.add(path.getLast().getNodeName()))
                    shortestPaths2.add(path);

            double currentdiametervalue;
        	currentdiametervalue = CalculateDiameter(shortestPaths2);
        	if (Diameter < currentdiametervalue)
        		Diameter = currentdiametervalue;

        	if (checkedCentralities[7])
        		BetweennessMethods.updateBetweenness(shortestPaths, BetweennessVectorResults);

        	if (checkedCentralities[11])
        		edgeBetweenness.updateEdgeBetweenness(root, shortestPaths);

        	singleShortestPaths.put(root, shortestPaths2);
        }   
        //execute each centrality
        for(Map.Entry<CyNode, List<ShortestPathList>> element : singleShortestPaths.entrySet()){
        	if (stop)
        		return;

        	CyNode root = element.getKey();
        	List<ShortestPathList> CentiScaPeSingleShortestPathVector = element.getValue();

        	if(checkedCentralities[1]){
        		ShortestPathList currentlist;
        		for (int j = 0; j < CentiScaPeSingleShortestPathVector.size(); j++) {
        			currentlist = CentiScaPeSingleShortestPathVector.get(j);
        			totalDist = totalDist + currentlist.getCost();
        		}
        	}
        	if(checkedCentralities[2]){
        		directedOutDegreeValues.put(root, (double) (network.getNeighborList(root,CyEdge.Type.OUTGOING).size()));
        		directedInDegreeValues.put(root, (double) (network.getNeighborList(root,CyEdge.Type.INCOMING).size()));
            }
            if(checkedCentralities[3])
                directedEccentrityValues.put(root, DirectedEccentricity.execute(CentiScaPeSingleShortestPathVector));

            if(checkedCentralities[4])
                directedRadialityValues.put(root, DirectedRadiality.execute(Diameter, totalnodecount, CentiScaPeSingleShortestPathVector));

            if(checkedCentralities[5])
                directedClosenessValues.put(root, DirectedCloseness.execute(CentiScaPeSingleShortestPathVector));

            if (checkedCentralities[8])
                CentroidMethods.updateCentroid(CentiScaPeSingleShortestPathVector, root, totalnodecount, CentroidVectorofNodes, CentroidVectorResults);

            if(checkedCentralities[9]){                
                List<CyNode> neighbors = network.getNeighborList(root, CyEdge.Type.OUTGOING);
                for(CyNode neighbor : neighbors)
                    adjacencyMatrixOfNetwork[k][nodeList.indexOf(neighbor)] = 1.0 ;

                k++;
            }
                
            menu.message("Computing shortest paths for node " + ++nodeworked + " of " + totalnodecount);
        }
        if (checkedCentralities[8])
            CentroidMethods.computeCentroid(CentroidVectorResults, totalnodecount, CentroidVectorofNodes);
        
        menu.endOfComputation(totalnodecount);
        centralities.clear();
        
        //put in table
        if(checkedCentralities[0])
            putValuesinTable(network, directedCentralities[0], Diameter);

        if(checkedCentralities[1]){
            double average = totalDist / (totalnodecount * (totalnodecount - 1));
            putValuesinTable(network, directedCentralities[1], average);
        }
        if(checkedCentralities[2]){
            putValuesinTable(network, directedCentralities[2], directedOutDegreeValues, centralities);
            putValuesinTable(network, "InDegree", directedInDegreeValues, centralities);
        }
        if(checkedCentralities[3])
            putValuesinTable(network, directedCentralities[3], directedEccentrityValues, centralities);

        if(checkedCentralities[4])
            putValuesinTable(network, directedCentralities[4], directedRadialityValues, centralities);

        if(checkedCentralities[5])
            putValuesinTable(network, directedCentralities[5], directedClosenessValues, centralities);

        if(checkedCentralities[6]){
            Set stressSet = stressMap.entrySet();
            for (Iterator i = stressSet.iterator(); i.hasNext();) {
                Map.Entry currentmapentry = (Map.Entry) i.next();
                long currentnodeSUID = (Long) currentmapentry.getKey();
                CyNode currentnode = network.getNode(currentnodeSUID);
                double currentstress = (double) (Double) (currentmapentry.getValue());
                directedStressValues.put(currentnode, currentstress);
            }
            putValuesinTable(network, directedCentralities[6], directedStressValues, centralities);
        }
        if (checkedCentralities[7] && displayBetweennessResults) {
        	nodeTable.createColumn(directedCentralities[7], Double.class, false);
        	double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0, currentvalue;

        	for (Iterator i = BetweennessVectorResults.iterator(); i.hasNext();) {
        		FinalResultBetweenness currentnodebetweenness = (FinalResultBetweenness) i.next();
        		double currentbetweenness = currentnodebetweenness.getBetweenness();

        		if (currentbetweenness < min) 
        			min = currentbetweenness;

        		if (currentbetweenness > max)
        			max = currentbetweenness;

        		totalsum = totalsum + currentbetweenness;

        		CyRow row = nodeTable.getRow(currentnodebetweenness.getNode().getSUID());
        		row.set(directedCentralities[7], new Double(currentbetweenness));
        	}
        	networkTable.createColumn(directedCentralities[7].split(" ")[0]+" Max value Dir", Double.class, false);
        	networkTable.createColumn(directedCentralities[7].split(" ")[0]+" min value Dir", Double.class, false);
        	double mean = totalsum / totalnodecount;
        	networkTable.createColumn(directedCentralities[7].split(" ")[0]+" mean value Dir", Double.class, false);
        	network.getRow(network).set(directedCentralities[7].split(" ")[0]+" Max value Dir", new Double(max));
        	network.getRow(network).set(directedCentralities[7].split(" ")[0]+" min value Dir", new Double(min));
        	network.getRow(network).set(directedCentralities[7].split(" ")[0]+" mean value Dir", new Double(mean));
        	centralities.add(new NodeCentrality(directedCentralities[7], mean, min, max));
        }
        if (checkedCentralities[8]) {
        	nodeTable.createColumn(directedCentralities[8], Double.class, false);
        	double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0, currentvalue;
        	for (Iterator i = CentroidVectorResults.iterator(); i.hasNext();) {
        		FinalResultCentroid currentnodeCentroid = (FinalResultCentroid) i.next();
        		double currentcentroid = currentnodeCentroid.getCentroid();

                if (currentcentroid < min)
                    min = currentcentroid;

                if (currentcentroid > max)
                    max = currentcentroid;

                totalsum = totalsum + currentcentroid;
                CyRow row = nodeTable.getRow(currentnodeCentroid.getNode().getSUID());
                row.set(directedCentralities[8], new Double(currentcentroid));
            }
            networkTable.createColumn(directedCentralities[8].split(" ")[0]+" Max value Dir", Double.class, false);
            networkTable.createColumn(directedCentralities[8].split(" ")[0]+" min value Dir", Double.class, false);
            double mean = totalsum / totalnodecount;
            networkTable.createColumn(directedCentralities[8].split(" ")[0]+" mean value Dir", Double.class, false);
            network.getRow(network).set(directedCentralities[8].split(" ")[0]+" Max value Dir", new Double(max));
            network.getRow(network).set(directedCentralities[8].split(" ")[0]+" min value Dir", new Double(min));
            network.getRow(network).set(directedCentralities[8].split(" ")[0]+" mean value Dir", new Double(mean));
      
            centralities.add(new NodeCentrality(directedCentralities[8], mean, min, max));
        }
        if(checkedCentralities[9])
            CalculateEigenVector.executeAndWriteValues(adjacencyMatrixOfNetwork,network, "EigenVector ", centralities, "Dir");

        if (checkedCentralities[10]) {
            nodeTable.createColumn(directedCentralities[10], Double.class, false);
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0, currentvalue;

            for (Iterator i = BetweennessVectorResults.iterator(); i.hasNext();) {
                FinalResultBetweenness currentnodebetweenness = (FinalResultBetweenness) i.next();

                double currentbetweenness = currentnodebetweenness.getBetweenness();
                CyNode root = currentnodebetweenness.getNode();
                List<CyNode> bridgingNeighborList = network.getNeighborList(root, CyEdge.Type.ANY);
                double bridgingCoefficient = 0;
                if(bridgingNeighborList.size() != 0){
                    double BCNumerator = (1/(double)(bridgingNeighborList.size()));
                    double BCDenominator = 0;                
                    for (CyNode bridgingNeighbor : bridgingNeighborList)
                        if(network.getNeighborList(bridgingNeighbor, CyEdge.Type.ANY).size() != 0)
                            BCDenominator = BCDenominator + 1/(double)(network.getNeighborList(bridgingNeighbor, CyEdge.Type.ANY).size());
                    if (BCDenominator!=0)
                        bridgingCoefficient = BCNumerator/BCDenominator;
                    else
                        bridgingCoefficient = 0.0;
                }
                double bridgingCentrality = bridgingCoefficient*currentbetweenness;
                if (bridgingCentrality < min)
                    min = bridgingCentrality;

                if (bridgingCentrality > max)
                	max = bridgingCentrality;

                totalsum = totalsum + bridgingCentrality;
                CyRow row = nodeTable.getRow(currentnodebetweenness.getNode().getSUID());
                row.set(directedCentralities[10], new Double(bridgingCentrality));
            }
            networkTable.createColumn(directedCentralities[10].split(" ")[0]+" Max value Dir", Double.class, false);
            networkTable.createColumn(directedCentralities[10].split(" ")[0]+" min value Dir", Double.class, false);
            double mean = totalsum / totalnodecount;
            networkTable.createColumn(directedCentralities[10].split(" ")[0]+" mean value Dir", Double.class, false);
            network.getRow(network).set(directedCentralities[10].split(" ")[0]+" Max value Dir", new Double(max));
            network.getRow(network).set(directedCentralities[10].split(" ")[0]+" min value Dir", new Double(min));
            network.getRow(network).set(directedCentralities[10].split(" ")[0]+" mean value Dir", new Double(mean));
            centralities.add(new NodeCentrality(directedCentralities[10], mean, min, max));

        }
        if (checkedCentralities[11]) {
        	CyTable edgeTable = network.getDefaultEdgeTable();
        	edgeTable.createColumn(directedCentralities[11], Double.class, false);
        	Map<CyEdge,Double> values = edgeBetweenness.getEdgeBetweennessMap();
        	Set<CyEdge> edges = values.keySet();

        	double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0, currentvalue;
        	Iterator it = edges.iterator();

        	while (it.hasNext()) {
        		CyEdge root = (CyEdge) it.next();
        		currentvalue = values.get(root);
        		if (currentvalue < min)
        			min = currentvalue;

        		if (currentvalue > max)
        			max = currentvalue;

        		totalsum = totalsum + currentvalue;
        		CyRow row = edgeTable.getRow(root.getSUID());
        		row.set(directedCentralities[11], new Double(currentvalue));
        	}

        	networkTable.createColumn(directedCentralities[11].split(" ")[0]+" Max value Dir", Double.class, false);
        	networkTable.createColumn(directedCentralities[11].split(" ")[0]+" min value Dir", Double.class, false);
        	double mean = totalsum / totalEdgeCount;
        	networkTable.createColumn(directedCentralities[11].split(" ")[0]+" mean value Dir", Double.class, false);
        	network.getRow(network).set(directedCentralities[11].split(" ")[0]+" Max value Dir", new Double(max));
        	network.getRow(network).set(directedCentralities[11].split(" ")[0]+" min value Dir", new Double(min));
        	network.getRow(network).set(directedCentralities[11].split(" ")[0]+" mean value Dir", new Double(mean));

        	// for embending centrality in Results Panel -- These two lines are enough
        	centralities.add(new NodeCentrality(directedCentralities[11], mean, min, max));
        }
        centiscapecore.createVisualizer(centralities);
    }

    public static void putValuesinTable(CyNetwork network, String heading,double result){     
        CyTable networkTable = network.getDefaultNetworkTable();
        networkTable.createColumn(heading, Double.class, false);
        network.getRow(network).set(heading, new Double(result));
    }
    
    public static void putValuesinTable(CyNetwork network, String heading, Map<CyNode,Double> values, Vector centralities){
        CyTable nodeTable = network.getDefaultNodeTable();
        nodeTable.createColumn(heading, Double.class, false);
        
        Set<CyNode> nodes = values.keySet();
        String networkattheading = heading;
            double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, totalsum = 0, currentvalue;
            Iterator it = nodes.iterator();
            
            while (it.hasNext()) {
                CyNode root = (CyNode) it.next();
                currentvalue = values.get(root);
                if (currentvalue < min)
                    min = currentvalue;

                if (currentvalue > max)
                    max = currentvalue;

                totalsum = totalsum + currentvalue;
                CyRow row = nodeTable.getRow(root.getSUID());
                row.set(heading, new Double(currentvalue));
            }
          
            if (!heading.equals("InDegree") && !heading.equals("OutDegree"))
               networkattheading = heading.split(" ")[0];

            CyTable networkTable = network.getDefaultNetworkTable();
            int totalnodecount = network.getNodeCount();
            networkTable.createColumn(networkattheading + " Max value Dir", Double.class, false);
            networkTable.createColumn(networkattheading + " min value Dir", Double.class, false);
            double mean = totalsum / totalnodecount;
            networkTable.createColumn(networkattheading + " mean value Dir", Double.class, false);
            network.getRow(network).set(networkattheading +" Max value Dir", new Double(max));
            network.getRow(network).set(networkattheading +" min value Dir", new Double(min));
            network.getRow(network).set(networkattheading +" mean value Dir", new Double(mean));

            centralities.add(new NodeCentrality(heading, mean, min, max));
    }
    
    private double CalculateDiameter(List<ShortestPathList> paths) {
        int max = 0;
        for (ShortestPathList path: paths)
            max = Math.max(max, path.getCost());

        return max;
    }
}