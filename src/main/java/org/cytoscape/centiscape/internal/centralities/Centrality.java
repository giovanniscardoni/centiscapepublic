package org.cytoscape.centiscape.internal.centralities;

import org.cytoscape.model.CyNetwork;

/**
 * @author admini
 */

public interface Centrality {
    public String getName();
    public double getMeanValue();
    public double getMinValue();
    public double getMaxValue();
    public String toString();
	public void showInPanelFor(CyNetwork network);
	public void removeFromPanel(CyNetwork network);
}