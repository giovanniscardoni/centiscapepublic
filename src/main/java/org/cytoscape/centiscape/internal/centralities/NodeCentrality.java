package org.cytoscape.centiscape.internal.centralities;

import java.util.HashMap;
import java.util.Map;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

/**
 * @author scardoni
 */

public class NodeCentrality  implements Centrality {
	private final String name;
	private final double mean;
	private final double min;
	private final double max;
	private final boolean directed;
	private final Map<CyNode, Double> values = new HashMap<CyNode, Double>();

	public NodeCentrality(String name, double mean, double min, double max) {
		this.name = name;  
		this.mean = mean;
		this.min = min;
		this.max = max;
		this.directed = false;
	}

	protected NodeCentrality(String name, boolean directed, NodeCentralityAlgorithm algorithm) {
		this.name = name;
		this.directed = directed;

		double min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY, sum = 0;

		int nodesCount = 0;
		for (CyNode root: algorithm.getNodes()) {
            double value = algorithm.computeAt(root);

            min = Math.min(min, value);
            max = Math.max(max, value);
            sum += value;
            nodesCount++;

            values.put(root, value);
        }

        this.mean = sum / nodesCount;
        this.min = min;
        this.max = max;
	}

	@Override
	public String getName() {
		return name; 
	}

	@Override
	public double getMeanValue() {
		return mean;
	}

	@Override
	public double getMinValue() {
		return min;
	}

	@Override
	public double getMaxValue() {
		return max;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void showInPanelFor(CyNetwork network) {
		CyTable nodeTable = network.getDefaultNodeTable();
	    CyTable networkTable = network.getDefaultNetworkTable();
	
	    String columnName = nodesColumnName();
	    if (nodeTable.getColumn(columnName) != null)
	    	nodeTable.deleteColumn(columnName);
	    nodeTable.createColumn(columnName, Double.class, false);
	    for (CyNode root: values.keySet()) {
	        CyRow row = nodeTable.getRow(root.getSUID());
	        row.set(columnName, values.get(root));
	    }

	    if (networkTable.getColumn(maxColumnName()) != null)
	    	networkTable.deleteColumn(maxColumnName());
	    networkTable.createColumn(maxColumnName(), Double.class, false);
	    if (networkTable.getColumn(minColumnName()) != null)
	    	networkTable.deleteColumn(minColumnName());
	    networkTable.createColumn(minColumnName(), Double.class, false);
	    if (networkTable.getColumn(meanColumnName()) != null)
	    	networkTable.deleteColumn(meanColumnName());
	    networkTable.createColumn(meanColumnName(), Double.class, false);
	    network.getRow(network).set(maxColumnName(), max);
	    network.getRow(network).set(minColumnName(), min);
	    network.getRow(network).set(meanColumnName(), mean);
	}

	@Override
	public void removeFromPanel(CyNetwork network) {
		CyTable nodeTable = network.getDefaultNodeTable();
		String columnName = nodesColumnName();
	    if (nodeTable.getColumn(columnName) != null)
	    	nodeTable.deleteColumn(columnName);
	
		CyTable networkTable = network.getDefaultNetworkTable();
		columnName = maxColumnName();
	    if (networkTable.getColumn(columnName) != null)
	    	networkTable.deleteColumn(columnName);
	
	    columnName = minColumnName();
	    if (networkTable.getColumn(columnName) != null)
	    	networkTable.deleteColumn(columnName);
	
	    columnName = meanColumnName();
	    if (networkTable.getColumn(columnName) != null)
	    	networkTable.deleteColumn(columnName);
	}

	private String direction() {
		return directed ? "dir" : "unDir";
	}

	private String meanColumnName() {
		return name + " mean value " + direction();
	}

	private String minColumnName() {
		return name + " min value " + direction();
	}

	private String maxColumnName() {
		return name + " max value " + direction();
	}

	private String nodesColumnName() {
		return name + ' ' + direction();
	}
}