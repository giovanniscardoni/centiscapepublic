package org.cytoscape.centiscape.internal;

public class CentiScaPeHasBeenStoppedException extends Exception {
	public CentiScaPeHasBeenStoppedException() {
		super("CentiScaPe has been stopped");
	}
}