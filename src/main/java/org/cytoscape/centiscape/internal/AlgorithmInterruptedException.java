package org.cytoscape.centiscape.internal;

public class AlgorithmInterruptedException extends Exception {
	public AlgorithmInterruptedException() {
		super("The user stopped CentiScaPe while it was running");
	}
}
