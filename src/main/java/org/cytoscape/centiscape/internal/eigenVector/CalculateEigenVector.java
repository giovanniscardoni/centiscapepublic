package org.cytoscape.centiscape.internal.eigenVector;

import java.util.List;

import org.cytoscape.centiscape.internal.centralities.Centrality;
import org.cytoscape.centiscape.internal.centralities.NodeCentrality;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;

/**
 * @author faizi
 */

public class CalculateEigenVector {

	public static void executeAndWriteValues(double[][] adjacencyMatrixOfNetwork, CyNetwork network, String centralityName, List<Centrality> centralities, String directionType){
		CyTable nodeTable = network.getDefaultNodeTable();
		List<CyNode> nodeList = network.getNodeList();

		Matrix A = new Matrix(adjacencyMatrixOfNetwork);
		EigenvalueDecomposition e = A.eig();
		Matrix V = e.getV();

		double[][] eigenVectors = V.getArray();

		nodeTable.createColumn(centralityName + directionType, Double.class, false);

		double min = Double.MAX_VALUE, max = -Double.MAX_VALUE, sum = 0;
		int numberOfNodes = nodeList.size();
		for (int j = 0; j < numberOfNodes; j++) {
			double value = eigenVectors[j][numberOfNodes - 1];

			min = Math.min(min, value);
			max = Math.max(max, value);

			sum += value;
			CyRow row = nodeTable.getRow(nodeList.get(j).getSUID());
			row.set(centralityName + directionType, value);
		}

		CyTable networkTable = network.getDefaultNetworkTable();
		networkTable.createColumn(centralityName+"Max value " + directionType, Double.class, false);
		networkTable.createColumn(centralityName+"min value " + directionType, Double.class, false);
		double mean = sum / network.getNodeCount();
		networkTable.createColumn(centralityName+"mean value " + directionType, Double.class, false);
		network.getRow(network).set(centralityName+"Max value " + directionType, max);
		network.getRow(network).set(centralityName+"min value " + directionType, min);
		network.getRow(network).set(centralityName+"mean value " + directionType, mean);

		centralities.add(new NodeCentrality(centralityName + directionType, mean, min, max));
	}
}