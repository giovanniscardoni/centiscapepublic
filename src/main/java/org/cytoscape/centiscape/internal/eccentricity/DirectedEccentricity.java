package org.cytoscape.centiscape.internal.eccentricity;

import java.util.List;

import org.cytoscape.centiscape.internal.ShortestPathList;

/**
 * @author faizi
 */

public class DirectedEccentricity {
	public static double execute(List<ShortestPathList> shortestPaths){
		if (shortestPaths.isEmpty())
			return 0.0;

		int max = 0;
		for (ShortestPathList path: shortestPaths)
			max = Math.max(max, path.getCost());

		return 1.0 / max;
	}
}