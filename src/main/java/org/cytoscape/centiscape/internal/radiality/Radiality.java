package org.cytoscape.centiscape.internal.radiality;

import org.cytoscape.centiscape.internal.ShortestPathList;
import org.cytoscape.centiscape.internal.ShortestPaths;
import org.cytoscape.centiscape.internal.centralities.NodeCentrality;
import org.cytoscape.centiscape.internal.centralities.NodeCentralityAlgorithm;
import org.cytoscape.model.CyNode;

public class Radiality extends NodeCentrality {

	public Radiality(final ShortestPaths shortestPaths) {
		super("Radiality", false, new NodeCentralityAlgorithm() {

			@Override
			public double computeAt(CyNode node) {
				int radiality = 0;
				int diameter = shortestPaths.getDiameter();

		    	for (ShortestPathList path: shortestPaths.getReducedFor(node)) {
		        	int distance = path.getCost();
		        	if (distance != 0)
		                radiality += diameter + 1 - distance;
		        }

		        //TODO e se nodesCount == 1 ?
		    	return radiality / (shortestPaths.getNodesCount() - 1.0);
			}

			@Override
			public Iterable<CyNode> getNodes() {
				return shortestPaths.getNodes();
			}
		});
	}
}