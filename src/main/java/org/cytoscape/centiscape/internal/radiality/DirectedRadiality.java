package org.cytoscape.centiscape.internal.radiality;

import java.util.List;

import org.cytoscape.centiscape.internal.ShortestPathList;

/**
 * @author faizi
 */

public class DirectedRadiality {

	public static double execute(double diameter, int totalNodeCount, List<ShortestPathList> shortestPathsList){
		double radiality = diameter * (totalNodeCount - 1);
		for (ShortestPathList pathList: shortestPathsList)
			radiality -= 1.0 / pathList.getCost();

		return radiality == 0.0 ? 0.0 : (1.0 / radiality);
	}
}