package org.cytoscape.centiscape.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanel;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.centiscape.internal.centralities.Centrality;
import org.cytoscape.centiscape.internal.visualizer.CentVisualizer;
import org.cytoscape.service.util.CyServiceRegistrar;

/**
 * @author scardoni
 */

public class Core {
	private final CyApplicationManager cyApplicationManager;
	private final CySwingApplication cyDesktopService;
	private final CyServiceRegistrar cyServiceRegistrar;
	private final CentiScaPeStartMenu menu;
	private final List<CentVisualizer> visualizers;

	public Core(CyActivator cyActivator) {
		this.cyApplicationManager = cyActivator.getApplicationManager();
		this.cyDesktopService = cyActivator.getDesktopService(); 
		this.cyServiceRegistrar = cyActivator.getServiceRegistrar();
		this.menu = createStartMenu(cyActivator);
		this.visualizers = new ArrayList<CentVisualizer>();
	}

	private CentiScaPeStartMenu createStartMenu(CyActivator cyActivator) {
		CentiScaPeStartMenu menu = new CentiScaPeStartMenu(cyActivator, this);
		cyServiceRegistrar.registerService(menu, CytoPanelComponent.class, new Properties());
		CytoPanel panelWest = cyDesktopService.getCytoPanel(CytoPanelName.WEST);
		panelWest.setSelectedIndex(panelWest.indexOfComponent(menu));
	
		return menu;
	}

	public void close() {
		for (CentVisualizer visualizer: visualizers)
			cyServiceRegistrar.unregisterService(visualizer, CytoPanelComponent.class);

		cyServiceRegistrar.unregisterService(menu, CytoPanelComponent.class);
	}

	public void createVisualizer(List<Centrality> centralities) {
		CentVisualizer visualizer = new CentVisualizer(cyApplicationManager, this);
		cyServiceRegistrar.registerService(visualizer, CytoPanelComponent.class, new Properties());
		CytoPanel panelEast = cyDesktopService.getCytoPanel(CytoPanelName.EAST);
		panelEast.setState(CytoPanelState.DOCK);
		panelEast.setSelectedIndex(panelEast.indexOfComponent(visualizer));
		visualizers.add(visualizer);
		visualizer.setEnabled(centralities);
	}

	public CyApplicationManager getCyApplicationManager() {
		return cyApplicationManager;
	}

	public CySwingApplication getCyDesktopService() {
		return cyDesktopService;
	}

	public void closeCurrentResultPanel(CentVisualizer resultPanel) {
		cyServiceRegistrar.unregisterService(resultPanel, CytoPanelComponent.class);
	}
}