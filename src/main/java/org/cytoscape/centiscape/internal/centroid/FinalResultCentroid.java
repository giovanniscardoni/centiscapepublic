/**
 * @author scardoni
 */

package org.cytoscape.centiscape.internal.centroid;

import org.cytoscape.model.CyNode;

public class FinalResultCentroid {
    private final double[] distances;
    private double centroid;
    private final CyNode node;
    
    public FinalResultCentroid(CyNode node, double centroid, int nodesCount) {
        this.node = node;
        this.centroid = centroid;
        this.distances = new double[nodesCount];
    }
    
    public void updatevector(int index, double distance) {
        this.distances[index] = distance;
    }
    
    public int getNumberOfDistances() {
        return distances.length;
    }
    
    public double getDistanceAt(int index) {
        return distances[index];
    }
    
    public void update(double value) {
        centroid = value;
    }

    @Override
    public String toString(){
        return "node SUID = " + node.getSUID() + " centroid = " + centroid;
    }

    public long getSUID() {
        return node.getSUID();
    }
    
    public double getCentroid() {
        return centroid;
    }
    
    public CyNode getNode() {
    	return node;
    }
}